#!/usr/bin/env bash


set -o errexit

GIT_URL="https://gitlab.com/datadrivendiscovery/d3m.git"
BRANCH="devel"
CURR_DIR="$(pwd)"
PUBLIC_PATH="$CURR_DIR/public"
VERSION_FILE="$CURR_DIR/versions.txt"

# Build for devel branch
./build-docs-for-ref.sh $GIT_URL $BRANCH $VERSION_FILE $PUBLIC_PATH
# Get all TAGS
for TAG in $(git ls-remote --tags "$GIT_URL" | sed -E 's/^[[:xdigit:]]+[[:space:]]+refs\/tags\/(.+)/\1/g' | python3 -c 'import sys; [sys.stdout.write(tag) for tag in sorted(sys.stdin, key=lambda t: tuple(map(int, t[1:].split("."))), reverse=True)]');
do
    ./build-docs-for-ref.sh $GIT_URL $TAG $VERSION_FILE $PUBLIC_PATH
done
python3 build-site.py $VERSION_FILE
cp -a "$CURR_DIR/static/papers.html" "$CURR_DIR/static/projects.html" "$CURR_DIR/static/wiki" "$PUBLIC_PATH"
exit 0
